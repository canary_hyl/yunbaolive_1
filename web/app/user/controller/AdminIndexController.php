<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-02-17
// +—————————————————————————————————————————————————————————————————————

namespace app\user\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use think\db\Query;

/**
 * Class AdminIndexController
 * @package app\user\controller
 *
 * @adminMenuRoot(
 *     'name'   =>'用户管理',
 *     'action' =>'default',
 *     'parent' =>'',
 *     'display'=> true,
 *     'order'  => 10,
 *     'icon'   =>'group',
 *     'remark' =>'用户管理'
 * )
 *
 * @adminMenuRoot(
 *     'name'   =>'用户组',
 *     'action' =>'default1',
 *     'parent' =>'user/AdminIndex/default',
 *     'display'=> true,
 *     'order'  => 10000,
 *     'icon'   =>'',
 *     'remark' =>'用户组'
 * )
 */
class AdminIndexController extends AdminBaseController{

    /**
     * 后台本站用户列表
     * @adminMenu(
     *     'name'   => '本站用户',
     *     'parent' => 'default1',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '本站用户',
     *     'param'  => ''
     * )
     */
    


    public function index(){

        $content = hook_one('user_admin_index_view');

        if (!empty($content)) {
            return $content;
        }
        
        $data = $this->request->param();
        $map=[];
        $map[]=['user_type','=',2];
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['create_time','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['create_time','<=',strtotime($end_time) + 60*60*24];
        }
        

        


        
        $ishot=isset($data['ishot']) ? $data['ishot']: '';
        if($ishot!=''){
            $map[]=['ishot','=',$ishot];
        }
        

        
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['user_login|user_nicename','like','%'.$keyword.'%'];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){

            $lianguid=getLianguser($uid);
            if($lianguid){
                $map[]=['id',['=',$uid],['in',$lianguid],'or'];
            }else{
                $map[]=['id','=',$uid];
            }
        }

        $configpub=getConfigPub();
        
        $nums=Db::name("user")->where($map)->count();
        
        $list = Db::name("user")
            ->where($map)
			->order("id desc")
			->paginate(20);
        
        $list->each(function($v,$k){
			
            $v['user_login']=m_s($v['user_login']);
            $v['mobile']=m_s($v['mobile']);
            $v['user_email']=m_s($v['user_email']);
            $v['avatar']=get_upload_path($v['avatar']);
            
            return $v;           
        });
        
        $list->appends($data);
        // 获取分页显示
        $page = $list->render();
        $this->assign('list', $list);
        $this->assign('page', $page);
        $this->assign('nowtime', time());
        $this->assign('name_coin', $configpub['name_coin']);
        $this->assign('name_votes', $configpub['name_votes']);
        $this->assign('nums', $nums);

        // 渲染模板输出
        return $this->fetch();
    }
    
    function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $user_login = DB::name('user')->where(["id"=>$id,"user_type"=>2])->value('user_login');
        $rs = DB::name('user')->where(["id"=>$id,"user_type"=>2])->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        
        
        /* 删除认证 */
        DB::name("user_auth")->where("uid='{$id}'")->delete();
        /* 删除直播记录 */
        DB::name("live")->where("uid='{$id}'")->delete();
        DB::name("live_record")->where("uid='{$id}'")->delete();

        /* 删除映票记录 */
        DB::name("user_voterecord")->where("uid='{$id}'")->delete();

        
        
   
        
        delcache("userinfo_".$id,"token_".$id);
        
        $this->success("删除成功！");
            
	}
    
    


    
    /* 热门 */
    function sethot(){
        
        $id = $this->request->param('id', 0, 'intval');
        $ishot = $this->request->param('ishot', 0, 'intval');
        
        $rs = DB::name('user')->where("id={$id}")->setField('ishot',$ishot);
        if(!$rs){
            $this->error("操作失败！");
        }
        DB::name("live")->where(array("uid"=>$id))->setField('ishot',$ishot);

        
        $this->success("操作成功！");
            
	}
    
  

  

    function add(){

		return $this->fetch();
	}
	function addPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();

            
			$user_login=$data['user_login'];

			if($user_login==""){
				$this->error("请填写手机号");
			}
            
            if(!checkMobile($user_login)){
                $this->error("请填写正确手机号");
            }
            
            $isexist=DB::name('user')->where(['user_login'=>$user_login])->value('id');
            if($isexist){
                $this->error("该账号已存在，请更换");
            }

            $data['mobile']=$user_login;
            
			$user_pass=$data['user_pass'];
			if($user_pass==""){
				$this->error("请填写密码");
			}
            
            if(!passcheck($user_pass)){
                $this->error("密码为6-20位字母数字组合");
            }
            
            $data['user_pass']=cmf_password($user_pass);
            

			$user_nicename=$data['user_nicename'];
			if($user_nicename==""){
				$this->error("请填写昵称");
			}

			$avatar=$data['avatar'];
			$avatar_thumb=$data['avatar_thumb'];
			if( ($avatar=="" || $avatar_thumb=='' ) && ($avatar!="" || $avatar_thumb!='' )){
                $this->error("请同时上传头像 和 头像小图  或 都不上传");
			}
            
            if($avatar=='' && $avatar_thumb==''){
                $data['avatar']='/default.jpg';
                $data['avatar_thumb']='/default_thumb.jpg';
            }

            $data['avatar']=set_upload_path($data['avatar']);
            $data['avatar_thumb']=set_upload_path($data['avatar_thumb']);
            
            $data['user_type']=2;
            $data['create_time']=time();
            
			$id = DB::name('user')->insertGetId($data);
            if(!$id){
                $this->error("添加失败！");
            }

            
            $this->success("添加成功！");
            
		}
	}
	function edit(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('user')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $data['user_login']=m_s($data['user_login']);
        $this->assign('data', $data);
        return $this->fetch();
	}
	
	function editPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
			$user_pass=$data['user_pass'];
			if($user_pass!=""){
				if(!passcheck($user_pass)){
                    $this->error("密码为6-20位字母数字组合");
                }
                
                $data['user_pass']=cmf_password($user_pass);
			}else{
                unset($data['user_pass']);
            }
            
			$user_nicename=$data['user_nicename'];
			if($user_nicename==""){
				$this->error("请填写昵称");
			}

            if(mb_substr($user_nicename, 0,1)=="="){
                $this->error("昵称内容非法");
            }

			$avatar=$data['avatar'];
			$avatar_thumb=$data['avatar_thumb'];

			if( ($avatar=="" || $avatar_thumb=='' ) && ($avatar!="" || $avatar_thumb!='' )){
                $this->error("请同时上传头像 和 头像小图  或 都不上传");
			}
            
            if($avatar=='' && $avatar_thumb==''){
                $data['avatar']='/default.jpg';
                $data['avatar_thumb']='/default_thumb.jpg';
            }

            $avatar_old=$data['avatar_old'];
            if($avatar_old!=$avatar){
                $data['avatar']=set_upload_path($data['avatar']);
            }
            
            $avatar_thumb_old=$data['avatar_thumb_old'];
            if($avatar_thumb_old!=$avatar_thumb){
                $data['avatar_thumb']=set_upload_path($data['avatar_thumb']);
            }

            unset($data['avatar_old']); 
            unset($data['avatar_thumb_old']); 
            
			$rs = DB::name('user')->update($data);
            if($rs===false){
                $this->error("修改失败！");
            }
            

            //查询用户信息存入缓存中
            $info=Db::name("user")
                        ->field('id,user_nicename,avatar,avatar_thumb,sex,signature,consumption,votestotal,province,city,birthday,user_status,location')
                        ->where("id={$data['id']} and user_type=2")
                        ->find();


            if($info){
                setcaches("userinfo_".$data['id'],$info);
            }
            
            $this->success("修改成功！");
		}
	}
}
